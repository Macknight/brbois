package com.mack.brasilbois.Controller;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

public abstract class State {
    protected OrthographicCamera cam;
    protected Vector3 mouse;
    protected  GameStateManager gms;

    protected State( GameStateManager gms){
        this.gms = gms;
        mouse = new Vector3();
        cam = new OrthographicCamera();
    }
    protected abstract  void handleInput();
    protected abstract  void update(float dt);
    protected abstract  void render(SpriteBatch sb);




}
