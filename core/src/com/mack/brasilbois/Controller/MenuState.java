package com.mack.brasilbois.Controller;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MenuState extends State {

    public MenuState(GameStateManager gms) {
        super(gms);
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch sb) {

    }
}
