package com.mack.brasilbois.model;

import java.util.List;

public class Player {

    private static final int INIT_HP = 40;
    private int health;
    private List<Card> playerDeck;

    private List<Card> graveYard;

    public Player( List<Card> playerDeck) {
        this.health = INIT_HP;
        this.playerDeck = playerDeck;
        this.graveYard = null;
    }

    public int checkDeckRemainingCards( ){

        if(playerDeck!=null){
            return playerDeck.size();
        }else{
            System.out.println("ERRO DECK NÃO INICIALIZADO");
            return 0;
        }

    }


}
