package com.mack.brasilbois.model;


import com.badlogic.gdx.graphics.Texture;

public class Card {

    public enum Faction {
        VERMELHO,
        AZUL,
        PESSOAS_DE_BEM,
        BANDIDAGEM,
        ARTISTAS,
        ATLETAS
    }
    public enum CardType{
        CRIATURA,
        ENCANTAMENTO,
        ARTEFEATO,
        MAGIA

    }
    public enum Organization{
        //VERMELHO
        PT,
        PSOL,
        PCDOB,
        PDT,
        LEVANTE,
        //AZUL
        PMDB,
        PSDB,
        MBL,
        //PESSOAS DE BEM
        FAMILIA_TRADICIONAL,
        POBRE_DE_DIREITA,
        BURGUES_SAFADO,
        //BANDIDAGEM
        PCC,
        COMANDO_VERMELHO,
        //ARTISTAS
        GLOBO,
        RECORD,
        SBT,
        REDETV,
        //ATLETAS
        BASKET,
        FUTEBOL,
        CORRIDA,
        NATACAO,


    }


    private int attack;
    private int defense;
    private int photo;
    private Faction faction;
    private CardType cardType;
    private Organization org;
    private String name;
    private Texture cardArt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }


    public Faction getFaction() {
        return faction;
    }

    public void setFaction(Faction faction) {
        this.faction = faction;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public Organization getOrg() {
        return org;
    }

    public void setOrg(Organization org) {
        this.org = org;
    }

    public Card(String name, int attack, int defense, int photo, Faction faction, CardType cardType, Organization org, Texture cardArt) {
        this.attack = attack;
        this.defense = defense;
        this.photo = photo;
        this.faction = faction;
        this.cardType = cardType;
        this.org = org;
        this.name = name;
        this.cardArt = cardArt;
    }



}
