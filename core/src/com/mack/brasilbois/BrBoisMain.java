package com.mack.brasilbois;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mack.brasilbois.Controller.GameStateManager;
import com.mack.brasilbois.Controller.MenuState;

import static com.mack.brasilbois.StaticValues.cardTest;

public class BrBoisMain extends ApplicationAdapter {

    public static final int WIDTH =  1000;
    public static final int HEIGHT= 700;

    public static final String TITLE = "Brasil Bois";

    private GameStateManager gsm;

    private SpriteBatch batch;

    Texture img;

    @Override
    public void create() {

        batch = new SpriteBatch();
        //create the gameStateManager
        gsm = new GameStateManager();
        //set the menu as current state
        gsm.push(new MenuState(gsm));

        img = new Texture("brasilboisLayout.png");
        cardTest();

    }



    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gsm.update(Gdx.graphics.getDeltaTime());
        gsm.render(batch);
    }


}
